
import org.slf4j.*;

public class Indemnisation{

	static double indemnite (double nbKm) throws IllegalArgumentException{
		double val=0;
		if (nbKm<0) {
			throw new IllegalArgumentException("nombre n�gatif");
		}
		if (nbKm<10)
			val=nbKm*1.5;
		
		else if (nbKm<40)
			val=15+(nbKm-10)*0.4;
		
		else if (nbKm<60)
			val=15+12+(nbKm-40)*0.55;
		
		else 
			val=15+12+11+((int)((nbKm-60)/20))*6.81;
		
		return val;
	}
	
	
	public static void main(String[] args) {
		
		Logger logger = LoggerFactory.getLogger("Indemnisation");
		logger.debug("Hello World");
		logger.trace("test");

		//int nbKm=32;
		
		//double value = indemnite(nbKm);
	

	}
}
