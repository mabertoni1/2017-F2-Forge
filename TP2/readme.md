# Infos étudiant :

Par binôme Nom Prénom:
 * Bertoni Marion
 * Walczyszyn Fabien

## TP 2

Le but de cet exercice est d'implementer un algorithme permettant de réaliser le calcul des indemnités kilométriques pour un trajet professionel.

Les regles sont les suivantes :

* Les trajets de moins de 10 Km sont remboursés à hauteur de 1.50€ du Km
* Les trajets de 10 Km à 39 Km sont remboursés à hauteur de 0.40€ du Km
* Les trajets de 40 Km à 60 Km sont remboursés à hauteur de 0.55€ du Km
* Par tranche de 20Km au dela de 60Km 6.81€

### 1 Développement et test unitaires

#### 1.1 Forkez le projet
Un fork permet d'avoir un acces total sur une copie du repository d'origine. Vous permettant de réaliser vos modifications et pousser ces modifications vers le projet d'orignie via une merge request. Le fait de forker vous permet de gérer vos `issues`, vos `pipelines`, `milestones` ... sans impacter directement le projet d'origine tout en concervant la possibilité d'y contribuer. [Documentation fork Project ](https://docs.gitlab.com/ce/gitlab-basics/fork-project.html).

#### 1.2 Implementer un algorithme calculant le montant de l'indeminté
* Votre programme prend en paramètre le nombre de kilomètre et retourne le montant de l'indemnité
* Pour réaliser ce programme vous utiliserez Java.
* Utilisez la librairie logback pour afficher des traces applicatives [Documentation installation logback](https://logback.qos.ch/download.html)


#### 1.3 Implémenter une classe de test permettant de valider votre algorithme
1. Quels sont les cas à tester ?
1. Quels sont les cas aux limites ?
1. Quels sont les cas d'erreur ?
1. Utiliser Junit pour réaliser votre classe de test
  * [Documentation installation junit4](http://junit.org/junit4/faq.html#started_1)
  * [Documentation utilisation junit4](http://junit.org/junit4/faq.html#atests_1)
1. Quels est le résultat pour les valeurs suivantes : 
  * -10 --> erreur d'arguments retournée
  * 0   -->        0
  * 0.1 -->     0.15
  * 17.123 -->     17.8492
  * 39.5 -->        26.8
  * 61  -->        38.0
  * 81  -->        44.81
  * 99  -->        44.81
