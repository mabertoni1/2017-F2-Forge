import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test; 

public class IndemnisationTest {
	@Test
	public void indemnisationTest() {
		
		assertEquals(Indemnisation.indemnite(0),0.0,0.0);
		assertEquals(Indemnisation.indemnite(0.1),0.15,0.0001);
		assertEquals(Indemnisation.indemnite(17.123),17.8492,0.0);
		assertEquals(Indemnisation.indemnite(39.5),26.8,0.0);
		assertEquals(Indemnisation.indemnite(61),38.0,0.0);
		assertEquals(Indemnisation.indemnite(81),44.81,0.0);
		assertEquals(Indemnisation.indemnite(99),44.81,0.0);
		
	}
	
	@Test 
	public void errorCases() {
		try {
			Indemnisation.indemnite(-10);
			fail("negative value did not throw exception");
		}catch(IllegalArgumentException e) {}
			
	}
	
	
	
	
	
}
